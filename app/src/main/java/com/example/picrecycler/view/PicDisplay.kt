package com.example.picrecycler.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import com.example.picrecycler.databinding.PicDisplayBinding
import com.example.picrecycler.model.PicRepo
import com.example.picrecycler.viewmodel.PicVMFactory
import com.example.picrecycler.viewmodel.PicViewModel

class PicDisplay: Fragment() {
    private val vmFactory:PicVMFactory = PicVMFactory(PicRepo())
    private val viewModel by viewModels<PicViewModel>() { vmFactory }
    private var _binding: PicDisplayBinding? = null
    private val binding: PicDisplayBinding get() = _binding!!
    private val theAdapter: PicAdapter by lazy {
        PicAdapter()
    }
    val TAG = "Pic Fragment"

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    )= PicDisplayBinding.inflate(
        inflater, container, false
    ).also{
        _binding = it
        iniViews()
        viewModel.getPics()
    }.root

    fun iniViews() {

        with(binding.rvKitties) {
            layoutManager = GridLayoutManager(context, 2)
            adapter = theAdapter
        }
        with(binding) {
            viewModel.pics.observe(viewLifecycleOwner){
            theAdapter.updateList(it)

            }
        }
    }

}