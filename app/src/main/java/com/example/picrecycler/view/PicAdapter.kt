package com.example.picrecycler.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.picrecycler.databinding.CatItemBinding
import com.example.picrecycler.databinding.PicDisplayBinding
import com.example.picrecycler.viewmodel.PicViewModel

class PicAdapter: RecyclerView.Adapter<PicAdapter.PicViewHolder>() {

    var list: MutableList<String> = mutableListOf()

    fun updateList(newList: List<String>) {
        val oldSize = list.size
        list.clear()
        notifyItemRangeChanged(0, oldSize)
        list.addAll(newList)
        notifyItemRangeChanged(0, newList.size)
    }

    class PicViewHolder(val binding: CatItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun displayImage(image: String) {
            binding.ivKitties.load(image)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PicViewHolder {
        return PicViewHolder(
            CatItemBinding.inflate(LayoutInflater.from(parent.context))
        )
    }

    override fun onBindViewHolder(holder: PicViewHolder, position: Int) {
        holder.displayImage(list[position])
    }

    override fun getItemCount(): Int = list.size
}