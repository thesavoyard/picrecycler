package com.example.picrecycler.model

import com.example.picrecycler.model.remote.PicService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import okhttp3.Dispatcher

class PicRepo {

   private val picService = PicService.getInstance()

    suspend fun getPics() = withContext(Dispatchers.IO){
        return@withContext picService.getPics()
    }
}