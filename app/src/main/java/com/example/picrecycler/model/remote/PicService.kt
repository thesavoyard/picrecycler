package com.example.picrecycler.model.remote


import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import retrofit2.http.GET
import retrofit2.http.Query

interface PicService {

    companion object {
       private const val BASE_URL = "https://shibe.online"
        private const val CATSENDPOINT = "/api/cats"

        fun getInstance(): PicService = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create()
    }

    @GET(CATSENDPOINT)
    suspend fun getPics(@Query("count")count: Int = 10) : List<String>

}