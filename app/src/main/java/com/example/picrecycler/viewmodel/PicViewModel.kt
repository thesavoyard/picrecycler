package com.example.picrecycler.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.picrecycler.model.PicRepo
import kotlinx.coroutines.launch

class PicViewModel(val repo: PicRepo): ViewModel() {

    private val _pics: MutableLiveData<List<String>> = MutableLiveData(listOf())
    val pics:LiveData<List<String>> get() = _pics

    fun getPics() {
        viewModelScope.launch {
            val result = repo.getPics()
            _pics.value = result
        }

    }

}