package com.example.picrecycler.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.picrecycler.model.PicRepo

class PicVMFactory(
   private val repo: PicRepo
):ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return PicViewModel(repo) as T
    }
}